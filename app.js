const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const async = require('async');
const walkdir = require('walkdir');
const measureTime = require('measure-time');

const host = "0.0.0.0";
const port = 3000;

const FS_TARGET_HDD = 'fs_HDD';
const FS_TARGET_SSD = 'fs_SSD';

const cache = {filelist:[],filecount:0,ssdReadTime:0,ssdWriteTime:0,hddReadTime:0,hddWriteTime:0};

app.use(express.static('frontend'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const preDirTargets = (startPath) => {
  let dPath = fs.readdirSync(startPath);
  // console.log('Start at '+startPath);
  // console.dir(dPath);

  for(let t in dPath) {
    let path = startPath+dPath[t];
    if(fs.statSync(path).isFile()) continue;
    let ssd = path.replace('fs_Source',FS_TARGET_SSD);
    let hdd = path.replace('fs_Source',FS_TARGET_HDD);
    if(!fs.existsSync(ssd)) fs.mkdirSync(ssd, {recursive:true});
    if(!fs.existsSync(hdd)) fs.mkdirSync(hdd, {recursive:true});
    preDirTargets(startPath+dPath[t]+"/");
  }
  return 1;
};


const scanSource = async (startPath) => {
  console.log("dirty dir hack ?");
  // console.dir(fs.readdirSync(startPath, {withFileTypes:true}));
  preDirTargets(startPath);
  walkdir.sync(startPath, (path, stat) => {
    cache.filelist.push(path);
    cache.filecount = cache.filelist.length;
  });
  return 'ok';
};

const getFreshRndNo = (max, pool) => {
  let rnd = parseInt(Math.random()*max)+1;
  if(pool.length>=max) pool = [];
  if(pool.indexOf(rnd)===-1) {
    pool.push(rnd);
    return rnd;
  } else {
    return getFreshRndNo(max, pool);
  }
};

const copyFile = async (src, dest) => {
  let fileStats = fs.statSync(src);
  // console.log('###### FILE STATS OUTPUT #######');
  // console.dir(fileStats);

  let isDir = fileStats.isDirectory();
  // console.log("ist dir: "+ isDir);

  if(isDir) {
    return {err:'cant copy directorys'}
  }
  
  let filename = path.basename(dest);
  
  // if(filename!='') {
  //   let toMkDir = dest.replace(filename,"");
  //   try {
      
  //     let dirExist = fs.statSync(toMkDir);
  //     // if(!dirExist.isDirectory()) {
  //     //   let createNew = fs.mkdirSync(toMkDir, {recursive:true});
  //     // }
  //   } catch(e) {
  //     // try {
  //       console.log("to Make Dir "+toMkDir);
  //       let mkERR = fs.mkdirSync(toMkDir, {recursive:true});
  //       console.log('have to make a directory');
  //       console.dir(mkERR);
  //     // } catch(e) {
  //       // console.log('some in the path is not healthy');
  //       // return {err:'cant create dir', toMkDir};
  //     // }
      
  //   }
  // } else {
  //   return {err:'no dir no file name - break here - copyFile'}
  // }  
  const getReadTime = measureTime();
  let readData = fs.readFileSync(src);
  const readTime = getReadTime();

  if(!(readData||false)) return {err: 'cant find source file', src, dest, stats};

  const getWriteTime = measureTime();
  let isWritten = fs.writeFileSync(dest, readData);
  const writeTime = getWriteTime();

  return {err:null, src, dest, fileStats, isWritten, filename, readTime, writeTime};

};

const aRndList = (max , len) => {
  if(max>len) len = max;
  let rndList = [];
  for(let i=0; i<=max; i++) {
    rndList.push( getFreshRndNo(len, rndList) );
  }
  return rndList;
};

const aLinearList = (max) => {
  let linList = [];
  for(let i=0;i<=max;i++) {
    linList.push(i);
  }
  return linList;
};

const init = async () => {
  console.log("start scann => ....");
  return await scanSource(__dirname+'/fs_Source/');
};

app.get('/getStats/', async (req,res) => {
  res.status(200).json(cache);
});

app.get('/reScanFilesystem/:sure', async (req, res) => {
  let ret = await init();
  res.status(200).json({returnCode:ret});
});

app.get('/getImage/:name', async (req, res) => {
  let imgName = req.params.name;
  let imPath = cache.filelist.filter( (e) => { if(e.indexOf(imgName)!=-1) return e; });
  if(imPath.length===1) {
    res.sendFile(imPath[0]);
  } else {
    res.status(200).send(`cant find image ${imgName}`);  
  }
});




app.get('/test/:blockdevice/:limit', async (req, res) => {
  let blockdevice = req.params.blockdevice.toLocaleLowerCase();
  let limit = parseInt(req.params.limit);
  let hssdd = (blockdevice==='ssd')? FS_TARGET_SSD : FS_TARGET_HDD;
  // let ddList = aRndList(limit, cache.filelist.length);
  let ddList = aLinearList(cache.filelist.length-1);
  let statList = [];
  let allTimes = measureTime();
  let allSize = 0;
  
  for(let r in ddList) {
    let fileSource = cache.filelist[ ddList[r] ];
    if(!(fileSource||false)) continue;
    let fileDest = fileSource.replace('fs_Source',hssdd);
    let re = await copyFile(fileSource,fileDest);
    if(re.err||false) continue;
    statList.push({ size: re.fileStats.size, read: re.readTime.millisecondsTotal, write: re.writeTime.millisecondsTotal });
    allSize += re.fileStats.size;
  }
  let alloverTime = allTimes();

  res.status(200).json({allSize, alloverTime, fileCount: ddList.length, statList});

  // let maxV = (cache.filelist.length<limit)? limit : cache.filelist.length;
  // if(maxV||false){
  //   for(let i=0;i<=maxV;i++) { 
  //     let rnd = getFreshRndNo(maxV,ddList);
  //     let fileSource = cache.filelist[rnd];
  //     if(!(fileSource||false)) continue;
  //     let fileDest = fileSource.replace('fs_Source',FS_TARGET_HDD);
  //     let re = await copyFile(fileSource,fileDest);
  //     if(re.err||false) continue;
  //     console.log('###### FILE TEST RETURN OUTPUT #######')
  //     console.dir(re);
  //     if(re.readTime.millisecondsTotal!=0 && re.writeTime.millisecondsTotal!=0) {
  //       let timeRead = re.fileStats.size/re.readTime.millisecondsTotal;
  //       let timeWrite = re.fileStats.size/re.writeTime.millisecondsTotal;
  //       if(blockdevice==='ssd') {
  //         cache.ssdReadTime = (cache.ssdReadTime===0)? timeRead : cache.ssdReadTime + timeRead;
  //         cache.ssdWriteTime = (cache.ssdWriteTime===0)? timeWrite : cache.ssdWriteTime + timeWrite;
  //       } else if(blockdevice==='hdd') {
  //         cache.hddReadTime = (cache.hddReadTime===0)? timeRead : cache.hddReadTime + timeRead;
  //         cache.hddWriteTime = (cache.hddWriteTime===0)? timeWrite : cache.hddWriteTime + timeWrite;
  //       }
  //     }
  //   }
  //   res.status(200).json(allTimes());
  // } else {
  //   res.status(404).json({err:'your params are wierd'})
  // }
});


app.listen(port, host);
console.log(`Running on http://${host}:${port}`);
console.log(new Date());


init();

module.exports = app;