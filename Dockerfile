FROM node:10-buster-slim

WORKDIR /var/www

RUN mkdir fs_HDD
RUN mkdir fs_SSD


COPY . .

COPY package*.json ./

RUN npm install

EXPOSE 3000

CMD [ "node", "app.js"]